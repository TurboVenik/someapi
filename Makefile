images:
	./scripts/build.sh
init:
	./scripts/init.sh
deploy:
	./scripts/deploy.sh

run: images deploy

swag:
	./scripts/swag.sh

setup:
	./scripts/setup.sh

test: setup
	./scripts/test.sh
sec: setup
	./scripts/sec.sh
fmt: setup
	./scripts/fmt.sh
vet: setup
	./scripts/vet.sh

all: test fmt vet sec

.DEFAULT_GOAL := all

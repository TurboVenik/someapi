package main

import (
	_ "someapi/api"
	"someapi/internal/config"
	"someapi/internal/controllers"
	"someapi/internal/logger"
)

func main() {
	logger.ConfigLogger()
	conf, err := config.LoadConfig()

	if err != nil {
		panic(err)
	}

	router := controllers.SetupRouter(conf)

	err = router.Run(conf.ServerAddr)
	if err != nil {
		panic(err)
	}
}

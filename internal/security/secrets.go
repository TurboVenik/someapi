package security

import (
	"github.com/pkg/errors"
	"io/ioutil"
	"someapi/internal/config"
)

const (
	JwtPublicKey = iota
)

const localSecretPath = "configs/docker_secrets/" // #nosec
const dockerSecretPath = "/run/secrets/"          // #nosec

var secretsMap = map[int]string{
	JwtPublicKey: "jwt_public_key",
}

type SecretExtractorI interface {
	getSecret(int) ([]byte, error)
}

type SecretExtractor struct {
	secretMap map[int][]byte
}

func GetSecretExtractor(conf config.Config) (*SecretExtractor, error) {
	extractor := &SecretExtractor{
		secretMap: map[int][]byte{},
	}
	var path string
	if conf.IsLocal {
		path = localSecretPath
	} else {
		path = dockerSecretPath
	}
	for k, v := range secretsMap {
		data, err := ioutil.ReadFile(path + v) // #nosec
		if err != nil {
			return nil, errors.Wrap(err, "failed to read the secret "+v)
		}
		extractor.secretMap[k] = data
	}

	return extractor, nil
}

func (e *SecretExtractor) getSecret(secretName int) ([]byte, error) {
	if bytes, ok := e.secretMap[secretName]; ok {
		return bytes, nil
	}
	return nil, errors.Errorf("there is not secret %d", secretName)
}

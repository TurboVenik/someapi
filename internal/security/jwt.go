package security

import (
	"github.com/golang-jwt/jwt"
	"github.com/pkg/errors"
	"someapi/internal/logger"
)

const TokenIsNotProvided = "token is not provided"
const InvalidToken = "invalid token"
const TokenExpired = "token expired"

type TokenError struct {
	Reason      string
	SecurityLog string
	Detail      error
}

func (e TokenError) Error() string {
	return e.Reason
}

func CheckToken(token *string, secretExtractor SecretExtractorI) (jwt.MapClaims, error) {
	if token == nil {
		return nil, TokenError{
			Reason:      TokenIsNotProvided,
			Detail:      errors.New(TokenIsNotProvided),
			SecurityLog: logger.EndpointNoToken}
	}
	tokenString := *token

	pk, err := secretExtractor.getSecret(JwtPublicKey)
	if err != nil {
		panic(errors.Wrap(err, "cat not get JWT public key"))
	}

	key, err := jwt.ParseRSAPublicKeyFromPEM(pk)
	if err != nil {
		panic(errors.Wrap(err, "invalid JWT public key"))
	}

	tok, err := jwt.Parse(tokenString, func(jwtToken *jwt.Token) (interface{}, error) {
		if _, ok := jwtToken.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, TokenError{
				Reason:      InvalidToken,
				Detail:      errors.New("invalid token alg"),
				SecurityLog: logger.EndpointInvalidToken}
		}
		return key, nil
	})

	if err != nil {
		switch v := err.(type) {
		case *jwt.ValidationError:
			if v.Errors == jwt.ValidationErrorExpired {
				return nil, TokenError{Reason: TokenExpired, Detail: err, SecurityLog: logger.EndpointExpiredToken}
			}
		}
		return nil, TokenError{Reason: InvalidToken, Detail: err, SecurityLog: logger.EndpointInvalidToken}
	}

	return tok.Claims.(jwt.MapClaims), nil
}

package security

import (
	"fmt"
	"github.com/golang-jwt/jwt"
	"github.com/pkg/errors"
	"os"
	"path"
	"runtime"
	"someapi/internal/logger"
	"testing"
	"time"
)

func init() {
	_, filename, _, _ := runtime.Caller(0)
	dir := path.Join(path.Dir(filename), "..")
	err := os.Chdir(dir)
	if err != nil {
		panic(err)
	}
}

const privateKey = `-----BEGIN RSA PRIVATE KEY-----
MIIBOgIBAAJAfhHN2AIDLnLVL8L08CazvEYCePXr54Zpul1bhlGzN/VQXRLEloi1
ngYPMx9bIVIuE4X7lQlCS/a3g4t3AAmU7QIDAQABAkAJRhwDWNqNDyt5kPjdtash
HXFPHqAU7wFWEUoLTnVFl262k80g5UKXgA9AlnbPHnx3b9Uk+VI0XLPzH3OK3FGh
AiEAu2497qmv5sQYgSbBsZPGyMCNB+9/y0k1j3UcVn6/PMkCIQCsMM8CmWeSHc8/
UXchNi17axB/azUT8Orgl3dtsoW9BQIhAI2j892GEk375uDC3yFJIbHYATv7+8Eu
I/GzeKTMx8wxAiEAkHofp9GpmmtSspEbQQ2hWfX6kL8WeRXjLfFJLwzktDUCIDU6
OjHqWDv++nAZLhKhm+Q6OurCTj1u4+9VjYR7wQ43
-----END RSA PRIVATE KEY-----`

const publicKey = `-----BEGIN PUBLIC KEY-----
MFswDQYJKoZIhvcNAQEBBQADSgAwRwJAfhHN2AIDLnLVL8L08CazvEYCePXr54Zp
ul1bhlGzN/VQXRLEloi1ngYPMx9bIVIuE4X7lQlCS/a3g4t3AAmU7QIDAQAB
-----END PUBLIC KEY-----`

const wrongPublicKey = `-----BEGIN PUBLIC KEY-----
MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAKRxxzehWxI/OGa2P5cILHnRaF4hyQ6I
ah1ol/jb/Cv2uod8zi3WwJZyBJlBOV11M8QkLayIf3lx4zWfHk4Ds00CAwEAAQ==
-----END PUBLIC KEY-----`

var hmacToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmb28iOiJiYXIiLCJuYmYiOjE0NDQ0Nzg0MDB9.u1riaD1rW97opCoAuRCTy4w58Br-Zk-bh7vLiRIsrpU"

type MockSecretExtractor struct {
	ResultMap map[int][]byte
}

type BadClaims string

func (BadClaims) Valid() error {
	return nil
}

func (e MockSecretExtractor) getSecret(secretName int) ([]byte, error) {
	v, ok := e.ResultMap[secretName]
	if !ok {
		return nil, errors.New("not key in secrets")
	}
	return v, nil
}

func createToken(expired bool, goodClaims bool) string {
	var exp int64
	if expired {
		exp = time.Now().Add(-10 * time.Minute).Unix()
	} else {
		exp = time.Now().Unix()
	}
	var claims jwt.Claims
	if goodClaims {
		claims = jwt.MapClaims{
			"uad": "uuid",
			"exp": exp,
		}
	} else {
		claims = BadClaims("bad")
	}
	token := jwt.NewWithClaims(jwt.GetSigningMethod("RS256"), claims)

	key, err := jwt.ParseRSAPrivateKeyFromPEM([]byte(privateKey))
	if err != nil {
		panic(err)
	}
	tokenString, err := token.SignedString(key)
	if err != nil {
		fmt.Println(err)
		panic(err)
	}
	return tokenString
}

type Case struct {
	Token       *string
	PublicKey   string
	Err         bool
	Reason      string
	SecurityLog string
	Claims      bool
}

func TestCheckTokenGoodJwt(t *testing.T) {
	token := createToken(false, true)
	claims, err := CheckToken(
		&token,
		MockSecretExtractor{
			ResultMap: map[int][]byte{
				JwtPublicKey: []byte(publicKey),
			},
		},
	)
	if err != nil {
		t.Error("Error must be nil")
	}
	if claims == nil || claims["uad"].(string) != "uuid" {
		t.Error("bad claims")
	}
}
func TestCheckTokenBadJwt(t *testing.T) {
	badToken := "bad token"
	expiredToken := createToken(true, true)
	goodToken := createToken(false, true)
	badClaimsToken := createToken(false, false)
	cases := []Case{
		{
			Token:       &expiredToken,
			PublicKey:   publicKey,
			Err:         true,
			Reason:      TokenExpired,
			SecurityLog: logger.EndpointExpiredToken,
			Claims:      false,
		},
		{
			Token:       &badToken,
			PublicKey:   wrongPublicKey,
			Err:         true,
			Reason:      InvalidToken,
			SecurityLog: logger.EndpointInvalidToken,
			Claims:      false,
		},
		{
			Token:       &goodToken,
			PublicKey:   wrongPublicKey,
			Err:         true,
			Reason:      InvalidToken,
			SecurityLog: logger.EndpointInvalidToken,
			Claims:      false,
		},
		{
			Token:       nil,
			PublicKey:   publicKey,
			Err:         true,
			Reason:      TokenIsNotProvided,
			SecurityLog: logger.EndpointNoToken,
			Claims:      false,
		},
		{
			Token:       &hmacToken,
			PublicKey:   publicKey,
			Err:         true,
			Reason:      InvalidToken,
			SecurityLog: logger.EndpointInvalidToken,
			Claims:      false,
		},
		{
			Token:       &badClaimsToken,
			PublicKey:   publicKey,
			Err:         true,
			Reason:      InvalidToken,
			SecurityLog: logger.EndpointInvalidToken,
			Claims:      false,
		},
	}

	for idx, item := range cases {

		claims, err := CheckToken(
			item.Token,
			MockSecretExtractor{
				ResultMap: map[int][]byte{
					JwtPublicKey: []byte(item.PublicKey),
				},
			},
		)

		if (err == nil) == item.Err {
			t.Errorf("[%d] wrong err %s == nil != %t", idx, err, item.Err)
		}
		tokenError := err.(TokenError)
		if tokenError.Reason != item.Reason {
			t.Errorf("[%d] wrong Reason %s != %s", idx, tokenError.Reason, item.Reason)
		}
		if tokenError.SecurityLog != item.SecurityLog {
			t.Errorf("[%d] wrong SecurityLog %s != %s", idx, tokenError.SecurityLog, item.SecurityLog)
		}
		if tokenError.Error() != tokenError.Reason {
			t.Errorf("[%d] bad error Error() %s != %s", idx, tokenError.Error(), tokenError.Reason)
		}
		if (claims == nil) == item.Claims {
			t.Error("bad claims")
		}
	}
}

func TestCheckPanicsBadKey(t *testing.T) {
	defer func() {
		if r := recover(); r == nil {
			t.Errorf("The code did not panic")
		}
	}()
	token := ""
	CheckToken(
		&token,
		MockSecretExtractor{
			ResultMap: map[int][]byte{
				JwtPublicKey: []byte(""),
			},
		},
	)
}

func TestCheckPanicsNotFoundKey(t *testing.T) {
	defer func() {
		if r := recover(); r == nil {
			t.Errorf("The code did not panic")
		}
	}()
	token := ""
	CheckToken(
		&token,
		MockSecretExtractor{
			ResultMap: map[int][]byte{},
		},
	)
}

package models

import (
	uuid "github.com/satori/go.uuid"
	"gorm.io/gorm"
	"time"
)

type Book struct {
	ID       uuid.UUID `json:"id" gorm:"type:uuid;primary_key;"`
	Title    string    `json:"title"`
	Price    *int      `json:"price"`
	AuthorID uuid.UUID `json:"author_id" gorm:"type:uuid;"`

	Author    Author    `json:"-"`
	CreatedAt time.Time `json:"-"`
}

func (u *Book) BeforeCreate(tx *gorm.DB) (err error) {
	u.ID = uuid.NewV4()
	return
}

package models

import (
	"fmt"
	"gorm.io/driver/postgres"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"someapi/internal/config"
)

func ConnectDataBase(conf config.Config) *gorm.DB {
	var database *gorm.DB
	var err error

	gormConfig := gorm.Config{}
	if conf.DBLogSql {
		gormConfig.Logger = logger.Default.LogMode(logger.Info)
	}

	if conf.DBUsePg {
		dsn := fmt.Sprintf(
			"host=%s user=%s password=%s dbname=%s port=%s",
			conf.DBHost,
			conf.DBUser,
			conf.DBPass,
			conf.DBName,
			conf.DBPort)
		fmt.Println(dsn)
		database, err = gorm.Open(postgres.Open(dsn), &gormConfig)
	} else {
		database, err = gorm.Open(sqlite.Open(conf.DBSqliteName), &gormConfig)
	}

	if err != nil {
		//panic("Failed to connect to database!")
	}

	err = database.AutoMigrate(&Book{}, &Author{})
	if err != nil {
		panic(err)
	}

	return database
}

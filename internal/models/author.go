package models

import (
	uuid "github.com/satori/go.uuid"
	"gorm.io/gorm"
)

type Author struct {
	ID   uuid.UUID `json:"id" gorm:"type:uuid;primary_key;"`
	Name string    `json:"name"`

	Books []Book `json:"-"`
}

func (u *Author) BeforeCreate(tx *gorm.DB) (err error) {
	u.ID = uuid.NewV4()
	return
}

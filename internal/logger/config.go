package logger

import "github.com/sirupsen/logrus"

func ConfigLogger() {
	logrus.SetFormatter(&logrus.TextFormatter{
		DisableColors: true,
		FullTimestamp: true,
	})
	logrus.SetReportCaller(true)
}

package logger

const DefaultLoggerName = "default"
const SecurityLoggerName = "security_events"

const EndpointNoToken = "ENDPOINT_NO_TOKEN" // #nosec
const EndpointInvalidToken = "ENDPOINT_INVALID_TOKEN"
const EndpointExpiredToken = "ENDPOINT_EXPIRED_TOKEN"
const EndpointGoodToken = "ENDPOINT_GOOD_TOKEN"

const GinContextLoggerKey = "CONTEXT_LOGGER"

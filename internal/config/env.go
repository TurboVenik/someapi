package config

import (
	"github.com/kelseyhightower/envconfig"
)

type Config struct {
	DBUsePg      bool   `envconfig:"DB_USE_PG"`
	DBHost       string `envconfig:"DB_HOST"`
	DBPort       string `envconfig:"DB_PORT" default:"5432"`
	DBUser       string `envconfig:"DB_USER"`
	DBPass       string `envconfig:"DB_PASS"`
	DBName       string `envconfig:"DB_NAME"`
	DBLogSql     bool   `envconfig:"DB_LOG_SQL"`
	DBSqliteName string `envconfig:"DB_SQLITE_NAME" default:"tmp/test.db"`
	IsLocal      bool   `envconfig:"IS_LOCAL"`
	ServerAddr   string `envconfig:"SERVER_ADDR" default:"0.0.0.0:8080"`
}

func LoadConfig() (Config, error) {
	config := Config{}
	err := envconfig.Process("", &config)
	if err != nil {
		panic(err)
	}
	return config, nil
}

package controllers

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"someapi/internal/models"
)

// AuthorBooks godoc
// @Summary AuthorBooks
// @Description AuthorBooks
// @ID AuthorBooks
// @Accept  json
// @Produce  json
// @Success 200 {object} AuthorBooksOutput
// @Param id path string true "Author ID"
// @Router /api/v1/authors/{id}/books [get]
// @Security ApiKeyAuth
func (s *Server) AuthorBooks(c *gin.Context) {
	var author models.Author

	if err := s.DB.Preload("Books").Where("id = ?", c.Param("id")).First(&author).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	res := AuthorBooksOutput{
		ID:          author.ID,
		Name:        author.Name,
		BooksAmount: len(author.Books),
		Books:       author.Books,
	}

	c.JSON(http.StatusOK, gin.H{"data": res})
}

// BookWithAuthor godoc
// @Summary BookWithAuthor
// @Description BookWithAuthor
// @ID BookWithAuthor
// @Accept  json
// @Produce  json
// @Success 200 {object} ExpandedBooksOutput
// @Param id path string true "Book ID"
// @Router /api/v1/custom/books/{id} [get]
// @Security ApiKeyAuth
func (s *Server) BookWithAuthor(c *gin.Context) {
	var book models.Book

	if err := s.DB.Joins("Author").First(&book, "books.id = ?", c.Param("id")).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}
	res := ExpandedBooksOutput{
		Title:    book.Title,
		Author:   book.Author,
		Price:    book.Price,
		AuthorID: book.AuthorID,
	}
	c.JSON(http.StatusOK, gin.H{"data": res})
}

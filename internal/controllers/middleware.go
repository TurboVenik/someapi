package controllers

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"net/http"
	"someapi/internal/logger"
	"someapi/internal/security"
	"time"
)

func (s *Server) Recover(c *gin.Context, _ interface{}) {
	c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"detail": "Internal Server Error"})
}

type jwtHeader struct {
	Token *string `header:"X-TOKEN"`
}

func (s *Server) JwtMiddleware(c *gin.Context) {
	h := jwtHeader{}

	err := c.ShouldBindHeader(&h)

	if err != nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"detail": "Token is not provided"})
		return
	}

	claims, err := security.CheckToken(h.Token, s.secretExtractor)
	if err != nil {
		switch v := err.(type) {
		case security.TokenError:
			s.GetSecurityLogger(c).Error(v.SecurityLog)
			s.GetLogger(c).Error(v.Detail)
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"detail": err.Error()})
			return
		default:
			s.GetSecurityLogger(c).Error(logger.EndpointInvalidToken)
			s.GetLogger(c).Error(err)
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"detail": "invalid token"})
			return
		}
	}

	s.UpdateLogger(c, "userid", claims["uad"].(string))
	s.GetSecurityLogger(c).Info(logger.EndpointGoodToken)
	c.Next()
}

func (s *Server) LogMiddleware(c *gin.Context) {
	s.GetLogger(c)
	start := time.Now()

	c.Next()

	end := time.Now()

	var level logrus.Level
	if c.Writer.Status() >= 200 && c.Writer.Status() < 300 {
		level = logrus.InfoLevel
	} else {
		level = logrus.ErrorLevel
	}

	s.GetLogger(c).WithFields(
		logrus.Fields{
			"origination": c.Request.RemoteAddr,
			"url":         c.Request.URL,
			"method":      c.Request.Method,
			"status":      c.Writer.Status(),
			"latency":     end.Sub(start),
		}).Log(level)
}

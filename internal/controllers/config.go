package controllers

import (
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"gorm.io/gorm"
	_ "someapi/api"
	"someapi/internal/config"
	"someapi/internal/models"
	"someapi/internal/security"
)

type Server struct {
	DB              *gorm.DB
	secretExtractor security.SecretExtractorI
	Config          config.Config
}

// @title Some API
// @version 1.0
// @description Some test API

// @host 0.0.0.0:8080
// @BasePath /

// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name X-Token
func SetupRouter(conf config.Config) *gin.Engine {
	secretExtractor, err := security.GetSecretExtractor(conf)
	if err != nil {
		panic(err)
	}
	server := &Server{
		DB:              models.ConnectDataBase(conf),
		secretExtractor: secretExtractor,
		Config:          conf,
	}
	router := gin.New()

	router.Use(server.LogMiddleware)
	router.Use(gin.CustomRecovery(server.Recover))

	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	apiGroup := router.Group("/api/v1")

	apiGroup.Use(server.JwtMiddleware)
	{
		apiGroup.GET("/books", server.FindBooks)
		apiGroup.POST("/books", server.CreateBook)
		apiGroup.GET("/books/:id", server.FindBook)
		apiGroup.PATCH("/books/:id", server.UpdateBook)
		apiGroup.DELETE("/books/:id", server.DeleteBook)

		apiGroup.POST("/authors", server.CreateAuthor)
		apiGroup.GET("/authors", server.FindAuthors)

		apiGroup.GET("/authors/:id/books", server.AuthorBooks)
		apiGroup.GET("/custom/books/:id", server.BookWithAuthor)
	}
	return router
}

package controllers

import (
	uuid "github.com/satori/go.uuid"
	"someapi/internal/models"
)

type CreateBookInput struct {
	Title    string    `json:"title" binding:"required" example:"title"`
	AuthorID uuid.UUID `json:"author_id" binding:"required"`
	Price    *int      `json:"price" binding:"required"`
}

type UpdateBookInput struct {
	Title    string    `json:"title" example:"title"`
	AuthorID uuid.UUID `json:"author_id"`
	Price    *int      `json:"price" binding:"required"`
}

type CreateAuthorInput struct {
	Name string `json:"author" binding:"required" example:"author"`
}

type AuthorBooksOutput struct {
	ID          uuid.UUID     `json:"id"`
	Name        string        `json:"author" binding:"required"`
	BooksAmount int           `json:"books_amount" binding:"required"`
	Books       []models.Book `json:"books" binding:"required" `
}

type ExpandedBooksOutput struct {
	Title    string        `json:"title" example:"title"`
	Author   models.Author `json:"author"`
	AuthorID uuid.UUID     `json:"author_id"`
	Price    *int          `json:"price" binding:"required"`
}

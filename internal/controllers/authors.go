package controllers

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"someapi/internal/models"
)

// FindAuthors godoc
// @Summary FindAuthors
// @Description FindAuthors
// @ID FindAuthors
// @Accept  json
// @Produce  json
// @Success 200 {object} models.Author
// @Router /api/v1/authors [get]
// @Security ApiKeyAuth
func (s *Server) FindAuthors(c *gin.Context) {
	var authors []models.Author
	s.DB.Find(&authors)

	c.JSON(http.StatusOK, gin.H{"data": authors})
}

// CreateAuthor godoc
// @Summary CreateAuthor
// @Description CreateAuthor
// @ID CreateAuthor
// @Accept  json
// @Produce  json
// @Param data body controllers.CreateAuthorInput true "body data".
// @Success 200 {object} models.Author
// @Router /api/v1/authors [post]
// @Security ApiKeyAuth
func (s *Server) CreateAuthor(c *gin.Context) {
	// Validate input
	var input CreateAuthorInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Create book
	author := models.Author{Name: input.Name}
	s.DB.Create(&author)

	c.JSON(http.StatusOK, gin.H{"data": author})
}

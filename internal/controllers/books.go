package controllers

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"someapi/internal/models"
)

// FindBooks godoc
// @Summary FindBooks
// @Description FindBooks
// @ID FindBooks
// @Accept  json
// @Produce  json
// @Success 200 {object} models.Book
// @Router /api/v1/books [get]
// @Security ApiKeyAuth
func (s *Server) FindBooks(c *gin.Context) {
	log := s.GetLogger(c)
	log.Info("FindBooks")

	var books []models.Book
	s.DB.Find(&books)

	c.JSON(http.StatusOK, gin.H{"data": books})
}

// CreateBook godoc
// @Summary CreateBook
// @Description FindBooks
// @ID CreateBook
// @Accept  json
// @Produce  json
// @Param data body controllers.CreateBookInput true "body data".
// @Success 200 {object} models.Book
// @Router /api/v1/books [post]
// @Security ApiKeyAuth
func (s *Server) CreateBook(c *gin.Context) {
	// Validate input
	var input CreateBookInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Create book
	book := models.Book{Title: input.Title, AuthorID: input.AuthorID, Price: input.Price}
	s.DB.Create(&book)

	c.JSON(http.StatusOK, gin.H{"data": book})
}

// FindBook godoc
// @Summary FindBook
// @Description FindBook
// @ID FindBook
// @Accept  json
// @Produce  json
// @Param id path string true "Book id"
// @Success 200 {object} models.Book
// @Router /api/v1/books/{id} [get]
// @Security ApiKeyAuth
func (s *Server) FindBook(c *gin.Context) { // Get model if exist
	var book models.Book

	if err := s.DB.Where("id = ?", c.Param("id")).First(&book).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": book})
}

// UpdateBook godoc
// @Summary UpdateBook
// @Description UpdateBook
// @ID UpdateBook
// @Accept  json
// @Produce  json
// @Param id path string true "Book id"
// @Param data body controllers.UpdateBookInput true "body data".
// @Success 200
// @Router /api/v1/books/{id} [patch]
// @Security ApiKeyAuth
func (s *Server) UpdateBook(c *gin.Context) {
	// Get model if exist
	var book models.Book
	if err := s.DB.Where("id = ?", c.Param("id")).First(&book).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	// Validate input
	var input UpdateBookInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
}

// DeleteBook godoc
// @Summary DeleteBook
// @Description DeleteBook
// @ID DeleteBook
// @Accept  json
// @Produce  json
// @Param id path string true "Book id"
// @Success 200
// @Router /api/v1/books/{id} [delete]
// @Security ApiKeyAuth
func (s *Server) DeleteBook(c *gin.Context) {
	// Get model if exist
	var book models.Book
	if err := s.DB.Where("id = ?", c.Param("id")).First(&book).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	s.DB.Delete(&book)

	c.JSON(http.StatusOK, gin.H{"data": true})
}

package controllers

import (
	"github.com/gin-gonic/gin"
	uuid "github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
	"someapi/internal/logger"
)

func (s *Server) GetLogger(c *gin.Context) *logrus.Entry {
	lg, exists := c.Get(logger.GinContextLoggerKey)
	if !exists {
		requestID := uuid.NewV4()
		requestLogger := logrus.WithFields(logrus.Fields{"traceid": requestID, "logger": logger.DefaultLoggerName})
		c.Set(logger.GinContextLoggerKey, requestLogger)
		return requestLogger
	}
	return lg.(*logrus.Entry)
}

func (s *Server) GetSecurityLogger(c *gin.Context) *logrus.Entry {
	return s.GetLogger(c).WithFields(logrus.Fields{"logger": logger.SecurityLoggerName})
}

func (s *Server) UpdateLogger(c *gin.Context, key, value string) *logrus.Entry {
	oldLogger := s.GetLogger(c)
	newLogger := oldLogger.WithFields(logrus.Fields{key: value})
	c.Set(logger.GinContextLoggerKey, newLogger)
	return newLogger
}
